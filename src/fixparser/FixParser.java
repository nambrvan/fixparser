/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fixparser;

import fixparser.bean.TestBean;
import fixparser.bean.TestBeanWithNonObjectList;
import fixparser.bean.TestWithListBean;
import fixparser.bean.olfa.refresh.ObeBpRequestBean;
import fixparser.composer.ObjectComposer;
import fixparser.parser.ObjectParser;

import static fixparser.parser.ObjectParser.PATH_SEPARATOR;
import static fixparser.parser.ObjectParser.SUB_TAG_SUFFIX;

import fixparser.tag.FixlikeBean;
import fixparser.tag.FixlikeField;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author user
 */
public class FixParser {

    private final static Map<String, Class<?>> messagesIn = new HashMap<>();
    private final static Map<Class<?>, Map<Integer, String>> allFields = new HashMap<>();


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

        //где-то мапятся сообщения с бинами
        messagesIn.put("UCUIA", TestWithListBean.class);
        messagesIn.put("UCUIA2", TestBean.class);
        messagesIn.put("QWERTY", TestBeanWithNonObjectList.class);
        messagesIn.put("BPR", ObeBpRequestBean.class);
        //for create with reflection
        ///////////////////////////////////////////////////////////////////////
        //на старте приложения берем все бины, пробегаемся по полям, чтобы замапить их на теги
        for (Class<?> clazz : messagesIn.values()) {
            Map<Integer, String> fields = new HashMap<>();
            findFields(clazz, fields, "");
            allFields.put(clazz, fields);
        }
        System.out.println(allFields.toString().replace(",", ",\n").replace("{", "\n{"));
        ///////////////////////////////////////////////////////////////////////
        final String fix = "35=UCUIA|10251=2041765824|13312=20190413-21:00:05.123|12345=name1|12543=10.5|12544=true";
        final String fix1 = "35=UCUIA|10251=2041765824|13312=20190413-21:00:05.123|12543=10.5|14444=2|19666=1111223|19312=20190413-21:00:05.123|19345=name1|19543=10.5|19547=true|19666=10431765824|19312=20180413-21:00:05.123|19543=20.5|19345=name2|19547=true|19547=false|12345=name7";
        final String fix2 = "35=UCUIA2|10251=2041765824|13312=20190413-21:00:05.123|12345=name1|12543=10.5|19666=111111|19312=20190413-21:00:05.123|19345=name1|19543=10.5|19547=true";
        final String fix3 = "35=QWERTY|19666=2041765824|19345=20190413-21:00:05.123|19999=8|19998=1|19998=1|19998=1|19998=1|19998=1|19998=2|19998=3|19998=4";
        final String fix4 = "35=BPR|10022=119985819|10023=N|10018=123|10144=1000000|10026=FirstName|10027=LastName|10024=U|10025=T|10149=N|10201=lastfirst123|10138=AF|10139=asdh kjh jkhkj hkj904 89 jkh2 k|10140=Kabul|10141=1213|10142=+3434 02394-12 0-84-2308|10145=hgjhgh@jhgjhg.com";
///////////////////////////////////////////////////////////////////////
        //приходит сообщение, парсим его
        Object o = callParse(fix);
        Object o1 = callParse(fix1);
        Object o2 = callParse(fix2);
        Object o3 = callParse(fix3);
        Object o4 = callParse(fix4);

        System.out.println("========================================================================================");
        Object oo = callParse(callCompose(o));
        System.out.println(oo.equals(o));
        Object oo1 = callParse(callCompose(o1));
        System.out.println(oo1.equals(o1));
        Object oo2 = callParse(callCompose(o2));
        System.out.println(oo2.equals(o2));
        Object oo3 = callParse(callCompose(o3));
        System.out.println(oo3.equals(o3));
        Object oo4 = callParse(callCompose(o4));
        System.out.println(oo4.equals(o4));

    }

    private static void findFields(Class<?> clazz, Map<Integer, String> fields, String prefix) {
        for (Field field : clazz.getDeclaredFields()) {
            FixlikeField column = field.getAnnotation(FixlikeField.class);
            if ((field.getType().isAssignableFrom(List.class) || field.getType().isAssignableFrom(Set.class))
                    && field.getGenericType() instanceof ParameterizedType) {
                ParameterizedType ptype = (ParameterizedType) field.getGenericType();
                Class<?> subClazz = (Class<?>) ptype.getActualTypeArguments()[0];
                if (subClazz.isAnnotationPresent(FixlikeBean.class) && !subClazz.isEnum()) {
                    findFields((Class<?>) ptype.getActualTypeArguments()[0], fields, getPrefix(prefix) + field.getName());
                }
                if (fields.containsKey(column.tag())) {
                    throw new IllegalArgumentException("Dublicate " + column.tag());
                }
                fields.put(column.tag(), getPrefix(prefix) + field.getName());
                if (column.subTag() != -1) {
                    if (fields.containsKey(column.subTag())) {
                        throw new IllegalArgumentException("Dublicate " + column.subTag());
                    }
                    fields.put(column.subTag(), getPrefix(prefix) + field.getName() + SUB_TAG_SUFFIX);
                }

            } else if (!field.getType().isPrimitive() && !field.getType().isEnum()
                    && field.getType().isAnnotationPresent(FixlikeBean.class)) {
                findFields(field.getType(), fields, getPrefix(prefix) + field.getName());
            } else {
                if (fields.containsKey(column.tag())) {
                    throw new IllegalArgumentException("Dublicate " + column.tag());
                }
                fields.put(column.tag(), getPrefix(prefix) + field.getName());
            }
        }
    }

    private static Object callParse(String msg) throws Exception {
        String[] splited = msg.split("\\|");

        Class<?> clazz = messagesIn.get(splited[0].split("=")[1]);

        ObjectParser parser = new ObjectParser(splited, allFields.get(clazz), clazz);

        System.out.println("========================START RESULT");
        Object parse = parser.parse();
        System.out.println(parse.toString().replace(",", ",\n").replace("{", "\n{"));
        System.out.println("========================END RESULT");
        return parse;
    }

    private static String callCompose(Object in) throws Exception {

        ObjectComposer parser = new ObjectComposer(in);

        System.out.println("========================START RESULT");
        Object compose = parser.compose();
        System.out.println(compose);
        System.out.println("========================END RESULT");
        return compose.toString();
    }

    private static String getPrefix(String prefix) {
        return prefix + (prefix.length() > 0 ? PATH_SEPARATOR : "");
    }
}
