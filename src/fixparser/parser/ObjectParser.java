/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fixparser.parser;

import com.sun.javafx.binding.StringFormatter;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author user
 */
public class ObjectParser {

    public static final String PATH_SEPARATOR = ".";
    public static final String SUB_TAG_SUFFIX = ".!sub";

    private final Map<String, BeanContainer> beans;
    private final String[] msgTags;
    private final Map<Integer, String> fields;
    private final Class ownerClazz;

    public ObjectParser(String[] msgTags,
                        Map<Integer, String> fields,
                        Class clazz) throws InstantiationException, IllegalAccessException {
        this.msgTags = msgTags;
        this.fields = fields;
        this.ownerClazz = clazz;

        beans = new HashMap<>();
        beans.put("", new BeanContainer(clazz.newInstance()));
    }

    public Object parse() throws Exception {
        for (int i = 1; i < msgTags.length; i++) {
            String[] cur = msgTags[i].split("=");
            Integer key = Integer.parseInt(cur[0]);
            String value = cur[1];
            if (!fields.containsKey(key)) {
                throw new IllegalArgumentException(StringFormatter.format("Tag [%s] is absent", key).get());
            }
            try {
                setInnerValue(fields.get(key), 0, ownerClazz, value);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(StringFormatter.format("Unable to set tag [%s] with value [%s] for owner Class [%s]", key, value, ownerClazz).get());
                throw e;
            }
        }
        Object bean = beans.get("").bean;

        try {
            Method method = bean.getClass().getMethod("allFieldsSet");
            method.invoke(bean);
        } catch (NoSuchMethodException | SecurityException ex) {
            return bean;
        }

        return bean;
    }

    private void setInnerValue(String path, int lastSeparatorLocation, Class clazz, String value) throws Exception {
        int separatorLocation = path.indexOf(PATH_SEPARATOR, lastSeparatorLocation + 1);
        if (separatorLocation == -1) {
            BeanContainer container = beans.get(path.substring(0, lastSeparatorLocation));
            String fieldStr = path.substring(lastSeparatorLocation + (lastSeparatorLocation > 0 ? 1 : 0));
            Field field = clazz.getDeclaredField(fieldStr);
            field.setAccessible(true);
            setValue(field, container.bean, value);
            container.setted.add(field.getName());
            return;
        }
        if (path.substring(separatorLocation).endsWith(SUB_TAG_SUFFIX)) {
            Object ownerBean = beans.get(path.substring(0, lastSeparatorLocation)).bean;
            String curField = path.substring(lastSeparatorLocation, separatorLocation);
            Field declaredField = ownerBean.getClass().getDeclaredField(curField);
            declaredField.setAccessible(true);
            Collection collection = (Collection) declaredField.get(ownerBean);
            collection.add(value);
            return;
        }
        String curBeanKey = path.substring(0, separatorLocation);

        String owner = path.substring(0, lastSeparatorLocation);
        BeanContainer ownerContainer = beans.get(owner);

        String curField = path.substring(lastSeparatorLocation, separatorLocation);
        Field field = ownerContainer.bean.getClass().getDeclaredField(curField);
        field.setAccessible(true);

        if (!beans.containsKey(curBeanKey)) {
            if (!(field.getType().isAssignableFrom(List.class) || field.getType().isAssignableFrom(Set.class))) {
                Object newBean = field.getType().newInstance();
                field.set(ownerContainer.bean, newBean);
                beans.put(curBeanKey, new BeanContainer(newBean));
            } else if (!path.endsWith(SUB_TAG_SUFFIX)) {
                beans.put(curBeanKey, new BeanContainer(addNewBeanToCollection(ownerContainer, field)));
            }
        } else {
            BeanContainer curBeanContainer = beans.get(curBeanKey);
            if (curBeanContainer.setted.contains(path.substring(separatorLocation + 1))
                    && (field.getType().isAssignableFrom(List.class) || field.getType().isAssignableFrom(Set.class))) {
                beans.replace(curBeanKey, new BeanContainer(addNewBeanToCollection(ownerContainer, field)));
            }
        }
        setInnerValue(path, separatorLocation, beans.get(curBeanKey).bean.getClass(), value);
    }

    private Object addNewBeanToCollection(BeanContainer ownerContainer, Field field) throws IllegalAccessException, InstantiationException {
        Collection collection = (Collection) field.get(ownerContainer.bean);
        ParameterizedType ptype = (ParameterizedType) field.getGenericType();
        Class subClazz = (Class<?>) ptype.getActualTypeArguments()[0];
        Object newBean = subClazz.newInstance();
        collection.add(newBean);
        return newBean;
    }

    private static void setValue(Field field, Object bean, String value) throws IllegalArgumentException, IllegalAccessException, ParseException {
        if (field.getType().isEnum()) {
            Class<Enum> enm = (Class<Enum>) field.getType();
            Enum valueOf;
            try {
                Method method = enm.getMethod("resolve", int.class);
                valueOf = (Enum) method.invoke(null, Integer.getInteger(value));
            } catch (Exception ex) {
                valueOf = Enum.valueOf(enm, value);
            }
            field.set(bean, valueOf);
            return;
        }
        switch (field.getType().getSimpleName()) {
            case "String": {
                field.set(bean, value);
                break;
            }
            case "Integer": {
                field.set(bean, new Integer(value));
                break;
            }
            case "int": {
                field.setInt(bean, Integer.parseInt(value));
                break;
            }
            case "Long": {
                field.set(bean, new Long(value));
                break;
            }
            case "long": {
                field.setLong(bean, Long.parseLong(value));
                break;
            }
            case "Double": {
                field.set(bean, new Double(value));
                break;
            }
            case "double": {
                field.setDouble(bean, Double.parseDouble(value));
                break;
            }
            case "Boolean": {
                field.set(bean, parseBoolean(value));
                break;
            }
            case "boolean": {
                field.setBoolean(bean, parseBoolean(value));
                break;
            }
            case "float": {
                field.setFloat(bean, Float.parseFloat(value));
                break;
            }
            case "Float": {
                field.set(bean, new Float(value));
                break;
            }
            case "Date": {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd-HH:mm:ss.SSS");
                field.set(bean, format.parse(value));
                break;
            }
            case "List": {
                field.set(bean, new ArrayList(Integer.parseInt(value)));
                break;
            }
            case "Set": {
                field.set(bean, new HashSet(Integer.parseInt(value)));
                break;
            }
            default: {

                throw new IllegalArgumentException(StringFormatter.format("Unable to set value [%s] for field [%s]", value, field).get());
            }
        }
    }

    private static Boolean parseBoolean(String value) {
        switch (value) {
            case "Y":
            case "1":
                return Boolean.TRUE;
            case "N":
            case "0":
                return Boolean.FALSE;
            default:
                return Boolean.valueOf(value);
        }
    }

    private static class BeanContainer {

        Object bean;
        Set<String> setted = new HashSet<>();

        BeanContainer(Object bean) {
            this.bean = bean;
        }

        @Override
        public String toString() {
            return "BeanContainer{" + "bean=" + bean + ", setted=" + setted + '}';
        }
    }
}
