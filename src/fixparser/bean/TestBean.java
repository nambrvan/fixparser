/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fixparser.bean;

import fixparser.tag.FixlikeBean;

import java.util.Date;

import fixparser.tag.FixlikeField;

/**
 * @author user
 */
@FixlikeBean(message = "UCUIA2")
public class TestBean {

    @FixlikeField(tag = 10251)
    private Integer requestId;

    @FixlikeField(tag = 12345)
    private String name;

    @FixlikeField(tag = 13312)
    private Date date;

    @FixlikeField(tag = 12543)
    private double price;

    @FixlikeField(tag = 12544)
    private Boolean flag;

    @FixlikeField()
    private SubTestBean subTestBean;

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public SubTestBean getSubTestBean() {
        return subTestBean;
    }

    public void setSubTestBean(SubTestBean subTestBean) {
        this.subTestBean = subTestBean;
    }

    @Override
    public String toString() {
        return "TestBean{" + "requestId=" + requestId + ", name=" + name + ", date=" + date + ", price=" + price + ", flag=" + flag + ", subTestBean=" + subTestBean + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TestBean)) return false;

        TestBean testBean = (TestBean) o;

        if (Double.compare(testBean.price, price) != 0) return false;
        if (requestId != null ? !requestId.equals(testBean.requestId) : testBean.requestId != null) return false;
        if (name != null ? !name.equals(testBean.name) : testBean.name != null) return false;
        if (date != null ? !date.equals(testBean.date) : testBean.date != null) return false;
        if (flag != null ? !flag.equals(testBean.flag) : testBean.flag != null) return false;
        return subTestBean != null ? subTestBean.equals(testBean.subTestBean) : testBean.subTestBean == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = requestId != null ? requestId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (flag != null ? flag.hashCode() : 0);
        result = 31 * result + (subTestBean != null ? subTestBean.hashCode() : 0);
        return result;
    }
}
