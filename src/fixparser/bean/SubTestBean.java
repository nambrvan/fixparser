/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fixparser.bean;

import fixparser.tag.FixlikeBean;
import fixparser.tag.FixlikeField;
import java.util.Date;

/**
 *
 * @author user
 */
@FixlikeBean
public class SubTestBean {

    @FixlikeField(tag = 19666)
    private long id;

    @FixlikeField(tag = 19345)
    private String name;

    @FixlikeField(tag = 19312)
    private Date date;

    @FixlikeField(tag = 19543)
    private double price;

    @FixlikeField(tag = 19547)
    private boolean flag;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "SubTestBean{" + "id=" + id + ", name=" + name + ", date=" + date + ", price=" + price + ", flag=" + flag + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SubTestBean)) return false;

        SubTestBean that = (SubTestBean) o;

        if (id != that.id) return false;
        if (Double.compare(that.price, price) != 0) return false;
        if (flag != that.flag) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return date != null ? date.equals(that.date) : that.date == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (flag ? 1 : 0);
        return result;
    }
}
