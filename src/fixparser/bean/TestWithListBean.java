/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fixparser.bean;

import fixparser.tag.FixlikeBean;
import java.util.Date;
import java.util.List;
import fixparser.tag.FixlikeField;

/**
 *
 * @author user
 */
@FixlikeBean(message = "UCUIA")
public class TestWithListBean {

    @FixlikeField(tag = 10251)
    private Integer requestId;
    
    @FixlikeField(tag = 12345)
    private String name;
    
    @FixlikeField(tag = 13312)
    private Date date;
    
    @FixlikeField(tag = 12543)
    private double price;
    
    @FixlikeField(tag = 12544)
    private boolean flag;
    
    @FixlikeField(tag = 14444)
    private List<SubTestBean> subTestBeans;

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<SubTestBean> getSubTestBeans() {
        return subTestBeans;
    }

    public void setSubTestBeans(List<SubTestBean> subTestBeans) {
        this.subTestBeans = subTestBeans;
    }

    @Override
    public String toString() {
        return "ExampleTestWithListBean{" + "requestId=" + requestId + ", name=" + name + ", date=" + date + ", price=" + price + ", flag=" + flag + ", subTestBeans=" + subTestBeans + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TestWithListBean)) return false;

        TestWithListBean that = (TestWithListBean) o;

        if (Double.compare(that.price, price) != 0) return false;
        if (flag != that.flag) return false;
        if (requestId != null ? !requestId.equals(that.requestId) : that.requestId != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        return subTestBeans != null ? subTestBeans.equals(that.subTestBeans) : that.subTestBeans == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = requestId != null ? requestId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (flag ? 1 : 0);
        result = 31 * result + (subTestBeans != null ? subTestBeans.hashCode() : 0);
        return result;
    }
}
