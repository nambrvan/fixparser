/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fixparser.bean;

import fixparser.tag.FixlikeBean;
import fixparser.tag.FixlikeField;

import java.util.List;
import java.util.Set;

/**
 * @author user
 */
@FixlikeBean(message = "QWERTY")
public class TestBeanWithNonObjectList {

    @FixlikeField(tag = 19666)
    private long id;

    @FixlikeField(tag = 19345)
    private String name;

    @FixlikeField(tag = 19999, subTag = 19998)
    private List<Long> set;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getSet() {
        return set;
    }

    public void setSet(List<Long> set) {
        this.set = set;
    }

    @Override
    public String toString() {
        return "TestBeanWithNonObjectList{" + "id=" + id + ", name=" + name + ", set=" + set + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TestBeanWithNonObjectList)) return false;

        TestBeanWithNonObjectList that = (TestBeanWithNonObjectList) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return set != null ? set.equals(that.set) : that.set == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (set != null ? set.hashCode() : 0);
        return result;
    }
}
