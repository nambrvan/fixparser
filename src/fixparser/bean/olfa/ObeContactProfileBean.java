//package com.olfa.common.bean;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.ObjectInputStream;
//import java.io.ObjectOutputStream;
//import java.io.OutputStream;
//import java.sql.Timestamp;
//import java.util.Date;
//
//public class ObeContactProfileBean extends ObeProfileBean {
//
//	private String title;
//	private String country;
//	private String address;
//	private String city;
//	private String zip;
//	private String phoneMobile;
//	private String phoneFix;
//	private String email;
//	
//	public ObeContactProfileBean(long id, long key, String title, String country, String address, String city, String zip, String phoneMobile, String phoneFix, String email) {
//		this(id, key, new Timestamp(System.currentTimeMillis()), null, title, country, address, city, zip, phoneMobile, phoneFix, email);
//	}
//	
//	public ObeContactProfileBean(long id, long key, Date startTime, Date endTime, String title, String country, String address, String city, String zip, String phoneMobile, String phoneFix, String email){
//		super(id, key, startTime, endTime);
//		this.title = title;
//		this.country = country;
//		this.address = address;
//		this.city = city;
//		this.zip = zip;
//		this.phoneMobile = phoneMobile;
//		this.phoneFix = phoneFix;
//		this.email = email;
//	}
//
//	public long getUserId(){
//		return super.getId();		
//	}
//	
//	public long getUserKey(){
//		return super.getKey();		
//	}
//	
//	public String getTitle() {
//		return title;
//	}
//
//	public String getCountry() {
//		return country;
//	}
//
//	public String getAddress() {
//		return address;
//	}
//
//	public String getCity() {
//		return city;
//	}
//
//	public String getZip() {
//		return zip;
//	}
//
//	public String getPhoneMobile() {
//		return phoneMobile;
//	}
//
//	public String getPhoneFix() {
//		return phoneFix;
//	}
//	
//	public String getEmail() {
//		return email;
//	}
//
//	protected void setUserId(long userId){
//		super.setId(userId);
//	}
//	
//	public void setUserKey(long userKey){
//		super.setKey(userKey);
//	}
//	
//	protected void setTitle(String title) {
//		this.title = title;
//	}
//
//	protected void setCoutnry(String country) {
//		this.country = country;
//	}
//
//	protected void setAddress(String address) {
//		this.address = address;
//	}
//
//	protected void setCity(String city) {
//		this.city = city;
//	}
//
//	protected void setZip(String zip) {
//		this.zip = zip;
//	}
//
//	protected void setPhoneMobile(String phoneMobile) {
//		this.phoneMobile = phoneMobile;
//	}
//
//	protected void setPhoneFix(String phoneFix) {
//		this.phoneFix = phoneFix;
//	}
//	
//	protected void setEmail(String email) {
//		this.email = email;
//	}
//	
//	//serialization
//	
//	protected ObeContactProfileBean() {}
//	
//	// IObeSerializable serialization hook
//	
//	@Override
//	public Object readObjectFrom(InputStream in) throws IOException,
//			ClassNotFoundException 
//	{
//		super.readObjectFrom(in);
//		readObject0(in);
//		return this;
//	}
//	
//	@Override
//	public void writeObjectIn(OutputStream out) throws IOException {
//		super.writeObjectIn(out);
//		writeObject0(out);
//	}
//	
//	//Java serialization hook
//	
//	private void writeObject(ObjectOutputStream out) throws IOException{
//		writeObject0(out);
//		
//	}
//	
//	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException{
//		readObject0(in);
//	}
//	
//	//serialization implementation
//
//	private void writeObject0(OutputStream out) throws IOException{
//		writeString(this.title, out);
//		writeString(this.country, out);
//		writeString(this.address, out);
//		writeString(this.city, out);
//		writeString(this.zip, out);
//		writeString(this.phoneMobile, out);
//		writeString(this.phoneFix, out);
//		writeString(this.email, out);
//	}
//	
//	private void readObject0(InputStream in) throws IOException, ClassNotFoundException{
//		this.title = readString(in);
//		this.country = readString(in);
//		this.address = readString(in);
//		this.city = readString(in);
//		this.zip = readString(in);
//		this.phoneMobile = readString(in);
//		this.phoneFix = readString(in);
//		this.email = readString(in);
//	}
//}
