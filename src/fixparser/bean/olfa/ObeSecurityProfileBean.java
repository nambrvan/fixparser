//package com.olfa.common.bean.security;
//
//import com.olfa.common.bean.ObeProfileBean;
//import com.olfa.common.bean.enumeration.security.ObeGrantEnum;
//
//import java.io.*;
//import java.sql.Timestamp;
//import java.util.Date;
//import java.util.HashSet;
//import java.util.Set;
//
///**
//  * @author Denis Denisenko <denis.denisenko@olfatrade.com>
// */
//public class ObeSecurityProfileBean extends ObeProfileBean {
//
//    private Set<ObeGrantEnum> excludedGrants = new HashSet<>();
//
//    public ObeSecurityProfileBean(long id, long key, Set<ObeGrantEnum> excludedGrants) {
//        this(id, key, new Timestamp(System.currentTimeMillis()), null, excludedGrants);
//    }
//
//    public ObeSecurityProfileBean(long id, long key, Date startTime, Date endTime, Set<ObeGrantEnum> excludedGrants) {
//        super(id, key, startTime, endTime);
//        this.excludedGrants = excludedGrants;
//    }
//
//    protected ObeSecurityProfileBean() {}
//
//    public long getKey(){
//        return super.getKey();
//    }
//
//    public void setKey(long userKey){
//        super.setKey(userKey);
//    }
//
//    public Set<ObeGrantEnum> getExcludedGrants() {
//        return excludedGrants;
//    }
//
//    public void setExcludedGrants(Set<ObeGrantEnum> excludedGrants) {
//        this.excludedGrants = excludedGrants;
//    }
//
//    @Override
//    public Object readObjectFrom(InputStream in) throws IOException,
//            ClassNotFoundException
//    {
//        super.readObjectFrom(in);
//        readObject0(in);
//        return this;
//    }
//
//    @Override
//    public void writeObjectIn(OutputStream out) throws IOException {
//        super.writeObjectIn(out);
//        writeObject0(out);
//    }
//
//    //Java serialization hook
//
//    private void writeObject(ObjectOutputStream out) throws IOException{
//        writeObject0(out);
//
//    }
//
//    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException{
//        readObject0(in);
//    }
//
//    //serialization implementation
//    private void writeObject0(OutputStream out) throws IOException {
//        writeCollection(excludedGrants, out);
//    }
//
//    private void readObject0(InputStream in) throws IOException, ClassNotFoundException {
//        excludedGrants = readCollection(Set.class, in);
//    }
//
//}
