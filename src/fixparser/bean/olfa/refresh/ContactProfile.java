/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fixparser.bean.olfa.refresh;

import fixparser.tag.FixlikeBean;
import fixparser.tag.FixlikeField;

/**
 *
 * @author user
 */
@FixlikeBean
public class ContactProfile {

    @FixlikeField(tag = 10137)
    private String title;

    @FixlikeField(tag = 10138)
    private String country;

    @FixlikeField(tag = 10139)
    private String address;

    @FixlikeField(tag = 10140)
    private String city;

    @FixlikeField(tag = 10141)
    private String zip;

    @FixlikeField(tag = 10142)
    private String phoneMobile;

    @FixlikeField(tag = 10143)
    private String phoneFix;

    @FixlikeField(tag = 10145)
    private String email;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhoneMobile() {
        return phoneMobile;
    }

    public void setPhoneMobile(String phoneMobile) {
        this.phoneMobile = phoneMobile;
    }

    public String getPhoneFix() {
        return phoneFix;
    }

    public void setPhoneFix(String phoneFix) {
        this.phoneFix = phoneFix;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "ContactProfile{" + "title=" + title + ", country=" + country + ", address=" + address + ", city=" + city + ", zip=" + zip + ", phoneMobile=" + phoneMobile + ", phoneFix=" + phoneFix + ", email=" + email + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContactProfile)) return false;

        ContactProfile that = (ContactProfile) o;

        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (country != null ? !country.equals(that.country) : that.country != null) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (zip != null ? !zip.equals(that.zip) : that.zip != null) return false;
        if (phoneMobile != null ? !phoneMobile.equals(that.phoneMobile) : that.phoneMobile != null) return false;
        if (phoneFix != null ? !phoneFix.equals(that.phoneFix) : that.phoneFix != null) return false;
        return email != null ? email.equals(that.email) : that.email == null;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (zip != null ? zip.hashCode() : 0);
        result = 31 * result + (phoneMobile != null ? phoneMobile.hashCode() : 0);
        result = 31 * result + (phoneFix != null ? phoneFix.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }
}
