/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fixparser.bean.olfa.refresh;

import fixparser.tag.FixlikeBean;
import fixparser.tag.FixlikeField;

/**
 *
 * @author user
 */
@FixlikeBean
public class UserProfile {

    @FixlikeField(tag = 10144)
    private long fatFingerLimit;

    @FixlikeField(tag = 10149)
    private boolean ableToTrade;

    public long getFatFingerLimit() {
        return fatFingerLimit;
    }

    public void setFatFingerLimit(long fatFingerLimit) {
        this.fatFingerLimit = fatFingerLimit;
    }

    public boolean isAbleToTrade() {
        return ableToTrade;
    }

    public void setAbleToTrade(boolean ableToTrade) {
        this.ableToTrade = ableToTrade;
    }

    @Override
    public String toString() {
        return "UserProfile{" + "fatFingerLimit=" + fatFingerLimit + ", ableToTrade=" + ableToTrade + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserProfile)) return false;

        UserProfile that = (UserProfile) o;

        if (fatFingerLimit != that.fatFingerLimit) return false;
        return ableToTrade == that.ableToTrade;
    }

    @Override
    public int hashCode() {
        int result = (int) (fatFingerLimit ^ (fatFingerLimit >>> 32));
        result = 31 * result + (ableToTrade ? 1 : 0);
        return result;
    }
}
