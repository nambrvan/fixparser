/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fixparser.bean.olfa.refresh;

import fixparser.tag.FixlikeBean;
import fixparser.tag.FixlikeField;

/**
 * @author user
 */
@FixlikeBean(message = "BPR")
public class ObeBpRequestBean {

    @FixlikeField(tag = 10022)
    private String requestId;

    @FixlikeField(tag = 10023)
    private RequestType requestType;

    @FixlikeField(tag = 10018)
    private Long personalKey;

    @FixlikeField(tag = 10024)
    private BPType bpType;

    @FixlikeField(tag = 10201)
    private String adoptedExternalReference;

    @FixlikeField(tag = 10026)
    protected String firstName;

    @FixlikeField(tag = 10027)
    protected String lastName;

    @FixlikeField
    private ContactProfile contactProfile;

    @FixlikeField
    private UserProfile userProfile;

    @FixlikeField
    private SecurityProfile securityProfile;

    @FixlikeField(tag = 10025)
    private UserRole userRole;

    public enum RequestType {

        N,//new (create)
        U,//update
        D,//delete
        A,//activate
        P;// passivate

    }

    public enum BPType {

        U,//user
        L,//liquidity provider
        P,//Primary Broker (PB)
        F,//Financial Institution (the bank with which the broker has correspondent relations: to deposit/withdraw client funds)
    }

    public enum UserRole {

        A,//administrator
        T,//trader
        M,//manager (an administrator of a managed unit)
        S,//sales person
        C,//DeskCliente
        B,//CSOB
        P,//AdministratorRO
        O,//Administrator IT
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public Long getPersonalKey() {
        return personalKey;
    }

    public void setPersonalKey(Long personalKey) {
        this.personalKey = personalKey;
    }

    public BPType getBpType() {
        return bpType;
    }

    public void setBpType(BPType bpType) {
        this.bpType = bpType;
    }

    public String getAdoptedExternalReference() {
        return adoptedExternalReference;
    }

    public void setAdoptedExternalReference(String adoptedExternalReference) {
        this.adoptedExternalReference = adoptedExternalReference;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ContactProfile getContactProfile() {
        return contactProfile;
    }

    public void setContactProfile(ContactProfile contactProfile) {
        this.contactProfile = contactProfile;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public SecurityProfile getSecurityProfile() {
        return securityProfile;
    }

    public void setSecurityProfile(SecurityProfile securityProfile) {
        this.securityProfile = securityProfile;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public void allFieldsSet() {
        check();
    }

    private void check() {
        switch (bpType) {
            case U: {
                switch (this.requestType) {
                    case N: {
                        if (userRole == null) {
                            throw new IllegalArgumentException("USER_ROLES");
                        }
                    }
                    case A: {
                        if (this.personalKey <= 0) {
                            throw new IllegalArgumentException("USER_PERSONAL_KEY");
                        }
                    }
                    case P: {
                        if (this.personalKey <= 0) {
                            throw new IllegalArgumentException("USER_PERSONAL_KEY");
                        }
                    }
                    case U:
                    case D:
                        break;
                    default:
                        throw new IllegalArgumentException("BP_REQUEST_TYPE");
                }
            }
            default:
                break;
        }
    }

    @Override
    public String toString() {
        return "ObeBpRequestBean{" + "requestId=" + requestId + ", requestType=" + requestType + ", personalKey=" + personalKey + ", bpType=" + bpType + ", adoptedExternalReference=" + adoptedExternalReference + ", firstName=" + firstName + ", lastName=" + lastName + ", contactProfile=" + contactProfile + ", userProfile=" + userProfile + ", securityProfile=" + securityProfile + ", userRole=" + userRole + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ObeBpRequestBean)) return false;

        ObeBpRequestBean that = (ObeBpRequestBean) o;

        if (requestId != null ? !requestId.equals(that.requestId) : that.requestId != null) return false;
        if (requestType != that.requestType) return false;
        if (personalKey != null ? !personalKey.equals(that.personalKey) : that.personalKey != null) return false;
        if (bpType != that.bpType) return false;
        if (adoptedExternalReference != null ? !adoptedExternalReference.equals(that.adoptedExternalReference) : that.adoptedExternalReference != null)
            return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (contactProfile != null ? !contactProfile.equals(that.contactProfile) : that.contactProfile != null)
            return false;
        if (userProfile != null ? !userProfile.equals(that.userProfile) : that.userProfile != null) return false;
        if (securityProfile != null ? !securityProfile.equals(that.securityProfile) : that.securityProfile != null)
            return false;
        return userRole == that.userRole;
    }

    @Override
    public int hashCode() {
        int result = requestId != null ? requestId.hashCode() : 0;
        result = 31 * result + (requestType != null ? requestType.hashCode() : 0);
        result = 31 * result + (personalKey != null ? personalKey.hashCode() : 0);
        result = 31 * result + (bpType != null ? bpType.hashCode() : 0);
        result = 31 * result + (adoptedExternalReference != null ? adoptedExternalReference.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (contactProfile != null ? contactProfile.hashCode() : 0);
        result = 31 * result + (userProfile != null ? userProfile.hashCode() : 0);
        result = 31 * result + (securityProfile != null ? securityProfile.hashCode() : 0);
        result = 31 * result + (userRole != null ? userRole.hashCode() : 0);
        return result;
    }
}
