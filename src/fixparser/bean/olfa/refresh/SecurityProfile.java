/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fixparser.bean.olfa.refresh;

import fixparser.tag.FixlikeBean;
import fixparser.tag.FixlikeField;
import java.util.Set;

/**
 *
 * @author user
 */
@FixlikeBean
public class SecurityProfile {

    @FixlikeField(tag = 10040, subTag = 10041)
    private Set<ObeGrantEnum> excludedGrants;

    public enum ObeGrantEnum {

        GRANT1,
        GRANT2,
        GRANT3,
        GRANT4,
        GRANT5;
    }

    public Set<ObeGrantEnum> getExcludedGrants() {
        return excludedGrants;
    }

    public void setExcludedGrants(Set<ObeGrantEnum> excludedGrants) {
        this.excludedGrants = excludedGrants;
    }

    @Override
    public String toString() {
        return "SecurityProfile{" + "excludedGrants=" + excludedGrants + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SecurityProfile)) return false;

        SecurityProfile that = (SecurityProfile) o;

        return excludedGrants != null ? excludedGrants.equals(that.excludedGrants) : that.excludedGrants == null;
    }

    @Override
    public int hashCode() {
        return excludedGrants != null ? excludedGrants.hashCode() : 0;
    }
}
