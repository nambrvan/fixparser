//package com.olfa.server.clientinterface.bean;
//
//import com.olfa.common.bean.ObeBean;
//import com.olfa.common.bean.ObeContactProfileBean;
//import com.olfa.common.bean.ObeUserActivateRequestBean;
//import com.olfa.common.bean.ObeUserCreateUpdateRequestBean;
//import com.olfa.common.bean.ObeUserDeleteRequestBean;
//import com.olfa.common.bean.ObeUserPassivateRequestBean;
//import com.olfa.common.bean.ObeUserProfileBean;
//import com.olfa.common.bean.enumeration.ObeRoleEnum;
//import com.olfa.common.bean.enumeration.security.ObeGrantEnum;
//import com.olfa.common.bean.security.ObeSecurityProfileBean;
//import com.olfa.common.exception.ObeIllegalArgumentException;
//import com.olfa.common.exception.ObeIllegalArgumentException.ILLEGAL_ARGUMENT_TYPE;
//import com.olfa.communication.bean.IObeCiFixBean;
//import com.olfa.communication.tobytearray.msgcomposer.field.IObeFixField;
//import com.olfa.communication.tobytearray.msgcomposer.field.IObeFixField.BOOLEAN_VALUES;
//import com.olfa.communication.tobytearray.msgcomposer.field.IObeFixField.BP_REQUEST_TYPES;
//import com.olfa.communication.tobytearray.msgcomposer.field.IObeFixField.BP_TYPES;
//import com.olfa.communication.tobytearray.msgcomposer.field.IObeFixField.USER_ROLES;
//
//import java.util.Set;
//
//public class ObeBpRequestFixBean implements IObeCiFixBean<ObeBean> {
//
//    protected String requestId;
//    protected IObeFixField.BP_REQUEST_TYPES requestType;
//    protected long personalKey;
//    protected IObeFixField.BP_TYPES bpType;
//    protected IObeFixField.USER_ROLES role;
//    protected String firstName;
//    protected String lastName;
//
//    protected _ContactProfileBean contactProfile;
//    protected _UserProfileBean userProfile;
//    protected _SecurityProfileBean securityProfile;
//
//    protected String adoptedExternalReference;
//
//    private static class _ContactProfileBean extends ObeContactProfileBean {
//
//        @Override
//        protected void setUserId(long userId) {
//            super.setUserId(userId);
//        }
//
//        @Override
//        public void setUserKey(long userKey) {
//            super.setUserKey(userKey);
//        }
//
//        @Override
//        protected void setTitle(String title) {
//            super.setTitle(title);
//        }
//
//        @Override
//        protected void setCoutnry(String country) {
//            super.setCoutnry(country);
//        }
//
//        @Override
//        protected void setAddress(String address) {
//            super.setAddress(address);
//        }
//
//        @Override
//        protected void setCity(String city) {
//            super.setCity(city);
//        }
//
//        @Override
//        protected void setZip(String zip) {
//            super.setZip(zip);
//        }
//
//        @Override
//        protected void setPhoneMobile(String phoneMobile) {
//            super.setPhoneMobile(phoneMobile);
//        }
//
//        @Override
//        protected void setPhoneFix(String phoneFix) {
//            super.setPhoneFix(phoneFix);
//        }
//
//        @Override
//        protected void setEmail(String email) {
//            super.setEmail(email);
//        }
//    }
//
//    private static class _UserProfileBean extends ObeUserProfileBean {
//
//        @Override
//        protected void setUserId(long userId) {
//            super.setUserId(userId);
//        }
//
//        @Override
//        public void setUserKey(long userKey) {
//            super.setUserKey(userKey);
//        }
//
//        @Override
//        protected void setFatFingerLimit(long fatFingerLimit) {
//            super.setFatFingerLimit(fatFingerLimit);
//        }
//    }
//
//    private static class _SecurityProfileBean extends ObeSecurityProfileBean {
//
//        @Override
//        public void setKey(long userKey) {
//            super.setKey(userKey);
//        }
//
//        @Override
//        public void setExcludedGrants(Set<ObeGrantEnum> excludedGrants) {
//            super.setExcludedGrants(excludedGrants);
//        }
//    }
//
//    public ObeBpRequestFixBean() {
//    }
//
//    @Override
//    public void setFixField(String key, String value) {
//        int fld = Integer.parseInt(key);
//        switch (fld) {
//            case IObeFixField.BP_REQUEST_ID: {
//                this.requestId = value;
//                break;
//            }
//            case IObeFixField.BP_REQUEST_TYPE: {
//                this.requestType = BP_REQUEST_TYPES.valueOf(value);
//                break;
//            }
//            case IObeFixField.BP_PERSONAL_UNIQUE_KEY: {
//                this.personalKey = Long.parseLong(value);
//                break;
//            }
//            case IObeFixField.BP_TYPE: {
//                this.bpType = BP_TYPES.valueOf(value);
//                break;
//            }
//            case IObeFixField.USER_ROLE: {
//                this.role = USER_ROLES.valueOf(value);
//                break;
//            }
//            case IObeFixField.USER_FIRST_NAME: {
//                this.firstName = value;
//                break;
//            }
//            case IObeFixField.USER_LAST_NAME: {
//                this.lastName = value;
//                break;
//            }
//            case IObeFixField.USER_TITLE: {
//                getContactProfileInstance().setTitle(value);
//                break;
//            }
//            case IObeFixField.COUNTRY_CODE: {
//                getContactProfileInstance().setCoutnry(value);
//                break;
//            }
//            case IObeFixField.EMAIL: {
//                getContactProfileInstance().setEmail(value);
//                break;
//            }
//            case IObeFixField.ADDRESS: {
//                getContactProfileInstance().setAddress(value);
//                break;
//            }
//            case IObeFixField.CITY: {
//                getContactProfileInstance().setCity(value);
//                break;
//            }
//            case IObeFixField.ZIP_CODE: {
//                getContactProfileInstance().setZip(value);
//                break;
//            }
//            case IObeFixField.PHONE_NUMBER_MOBILE: {
//                getContactProfileInstance().setPhoneMobile(value);
//                break;
//            }
//            case IObeFixField.PHONE_NUMBER_FIX: {
//                getContactProfileInstance().setPhoneFix(value);
//                break;
//            }
//            case IObeFixField.FAT_FINGER_LIMIT: {
//                getUserProfileInstance().setFatFingerLimit(Long.parseLong(value));
//                break;
//            }
//            case IObeFixField.ABLE_TO_TRADE: {
//                if (false == BOOLEAN_VALUES.valueOf(value).getMatchedBoolean()) {
//                    getSecurityProfileInstance().getExcludedGrants().add(ObeGrantEnum.ABLE_TO_TRADE);
//                }
//                break;
//            }
//            case IObeFixField.USER_ADOPTED_REFERENCE:
//            case IObeFixField.GROUP_ADOPTED_REFERENCE:
//                this.adoptedExternalReference = value;
//                break;
//            default: {
//                break;
//            }
//        }
//    }
//
//    protected _ContactProfileBean getContactProfileInstance() {
//        return this.contactProfile == null ? this.contactProfile = new _ContactProfileBean() : this.contactProfile;
//    }
//
//    protected _UserProfileBean getUserProfileInstance() {
//        return this.userProfile == null ? this.userProfile = new _UserProfileBean() : this.userProfile;
//    }
//
//    protected _SecurityProfileBean getSecurityProfileInstance() {
//        return this.securityProfile == null ? this.securityProfile = new _SecurityProfileBean() : this.securityProfile;
//    }
//
//    @Override
//    public void allFieldsSet() {
//        if (this.contactProfile != null) {
//            this.contactProfile.setUserKey(this.personalKey);
//        }
//        if (this.userProfile != null) {
//            this.userProfile.setUserKey(this.personalKey);
//        }
//        if (this.securityProfile != null) {
//            this.securityProfile.setKey(this.personalKey);
//        }
//    }
//
//    @Override
//    public ObeBean narrowToTarget() {
//        if (this.requestType == null) {
//            throw new ObeIllegalArgumentException(ILLEGAL_ARGUMENT_TYPE.EMPTY, "BP_REQUEST_TYPES");
//        } else if (bpType == null) {
//            throw new ObeIllegalArgumentException(ILLEGAL_ARGUMENT_TYPE.EMPTY, "BP_TYPES");
//        }
//        switch (bpType) {
//            case U: {
//                switch (this.requestType) {
//                    case N: {
//                        if (role == null) {
//                            throw new ObeIllegalArgumentException(ILLEGAL_ARGUMENT_TYPE.EMPTY, "USER_ROLES");
//                        }
//                    }
//                    case U: {
//                        return getObeUserCreateUpdateRequestBean();
//                    }
//                    case A: {
//                        if (this.personalKey <= 0) {
//                            throw new ObeIllegalArgumentException(ILLEGAL_ARGUMENT_TYPE.EMPTY, "USER_PERSONAL_KEY");
//                        }
//                        return new ObeUserActivateRequestBean(requestId, this.personalKey);
//                    }
//                    case P: {
//                        if (this.personalKey <= 0) {
//                            throw new ObeIllegalArgumentException(ILLEGAL_ARGUMENT_TYPE.EMPTY, "USER_PERSONAL_KEY");
//                        }
//                        return new ObeUserPassivateRequestBean(requestId, this.personalKey);
//                    }
//                    case D: {
//                        return getObeUserDeleteRequestBean();
//                    }
//                    default:
//                        throw new ObeIllegalArgumentException(ILLEGAL_ARGUMENT_TYPE.EMPTY, "BP_REQUEST_TYPE");
//                }
//            }
//            default:
//                break;
//        }
//        return null;
//    }
//
//    protected ObeBean getObeUserDeleteRequestBean() {
//        if (this.personalKey <= 0) {
//            throw new ObeIllegalArgumentException(ILLEGAL_ARGUMENT_TYPE.EMPTY, "USER_PERSONAL_KEY");
//        }
//        return new ObeUserDeleteRequestBean(requestId, this.personalKey);
//    }
//
//    protected ObeUserCreateUpdateRequestBean getObeUserCreateUpdateRequestBean() {
//        final ObeUserCreateUpdateRequestBean obeUserCreateUpdateRequestBean = new ObeUserCreateUpdateRequestBean(
//                requestId,
//                this.firstName,
//                this.lastName,
//                resolveUserRole(this.role),
//                this.personalKey, this.contactProfile, this.userProfile, getSecurityProfileInstance());
//        obeUserCreateUpdateRequestBean.setAdoptedReference(adoptedExternalReference);
//        return obeUserCreateUpdateRequestBean;
//    }
//
//    protected ObeRoleEnum resolveUserRole(USER_ROLES msgRole) {
//        switch (msgRole) {
//            case A: {
//                return ObeRoleEnum.ADMINISTRATOR;
//            }
//            case T: {
//                return ObeRoleEnum.TRADER;
//            }
//            case S: {
//                return ObeRoleEnum.SALES;
//            }
//            case M: {
//                return ObeRoleEnum.MANAGER;
//            }
//            case R: {
//                return ObeRoleEnum.TREASURY;
//            }
//            case C: {
//                return ObeRoleEnum.CLIENT_DESK;
//            }
//            case B: {
//                return ObeRoleEnum.CSOB;
//            }
//            case P: {
//                return ObeRoleEnum.ADMINISTRATOR_RO;
//            }
//            case O: {
//                return ObeRoleEnum.BACKOFFICE;
//            }
//            default: {
//                return null;
//            }
//        }
//    }
//}
