//package com.olfa.common.bean;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.ObjectInputStream;
//import java.io.ObjectOutputStream;
//import java.io.OutputStream;
//import java.sql.Timestamp;
//import java.util.Date;
//
//public class ObeUserProfileBean extends ObeProfileBean {
//
//	private long fatFingerLimit;
//
//	public ObeUserProfileBean(long id, long key, long fatFingerLimit) {
//		this(id, key, new Timestamp(System.currentTimeMillis()), null, fatFingerLimit);
//	}
//	
//	public ObeUserProfileBean(long id, long key, Date startTime, Date endTime, long fatFingerLimit) {
//		super(id, key, startTime, endTime);
//		this.fatFingerLimit = fatFingerLimit;
//	}
//
//	public long getUserId(){
//		return super.getId();		
//	}
//	
//	public long getUserKey(){
//		return super.getKey();		
//	}
//	
//	public long getFatFingerLimit() {
//		return this.fatFingerLimit;
//	}
//
//	protected void setUserId(long userId){
//		super.setId(userId);
//	}
//	
//	public void setUserKey(long userKey){
//		super.setKey(userKey);
//	}
//	
//	protected void setFatFingerLimit(long fatFingerLimit) {
//		this.fatFingerLimit = fatFingerLimit;
//	}
//
//	//serialization
//	
//	protected ObeUserProfileBean() {}
//	
//	// IObeSerializable serialization hook
//	
//	@Override
//	public Object readObjectFrom(InputStream in) throws IOException,
//			ClassNotFoundException 
//	{
//		super.readObjectFrom(in);
//		readObject0(in);
//		return this;
//	}
//	
//	@Override
//	public void writeObjectIn(OutputStream out) throws IOException {
//		super.writeObjectIn(out);
//		writeObject0(out);
//	}
//	
//	//Java serialization hook
//	
//	private void writeObject(ObjectOutputStream out) throws IOException{
//		writeObject0(out);
//		
//	}
//	
//	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException{
//		readObject0(in);
//	}
//	
//	//serialization implementation
//
//	private void writeObject0(OutputStream out) throws IOException{
//		writeInt(this.fatFingerLimit, out);
//	}
//	
//	private void readObject0(InputStream in) throws IOException, ClassNotFoundException{
//		this.fatFingerLimit = readInt(in);
//	}
//}
