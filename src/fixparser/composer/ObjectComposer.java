/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fixparser.composer;

import fixparser.tag.FixlikeBean;
import fixparser.tag.FixlikeField;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author user
 */
public class ObjectComposer {

    private final Object in;
    StringBuilder result = new StringBuilder();

    public ObjectComposer(Object in) {
        this.in = in;
    }

    public Object compose() throws Exception {
        Class<?> clazz = in.getClass();

        FixlikeBean clazzAnnotation = clazz.getAnnotation(FixlikeBean.class);
        result.append(35).append('=').append(clazzAnnotation.message());

        doCompose(in.getClass(), in);

        return result;
    }

    private void doCompose(Class<?> clazz, Object in) throws IllegalAccessException, ParseException {
        if (in == null) {
            return;
        }
        for (Field field : clazz.getDeclaredFields()) {
            FixlikeField annotation = field.getAnnotation(FixlikeField.class);
            field.setAccessible(true);
            if (annotation == null) {
                continue;
            }

            if (Collection.class.isAssignableFrom(field.getType())) {
                Collection<?> collection = (Collection<?>) field.get(in);
                if (collection == null) {
                    continue;
                }
                result.append('|').append(annotation.tag()).append('=').append(collection.size());
                for (Object object : collection) {
                    if (object.getClass().isAnnotationPresent(FixlikeBean.class)) {
                        doCompose(object.getClass(), object);
                    } else {
                        result.append('|').append(annotation.subTag()).append('=').append(object);
                    }
                }

            } else if (field.getType().isAnnotationPresent(FixlikeBean.class)) {
                doCompose(field.getType(), field.get(in));

            } else if (field.getType().isEnum() && field.get(in) != null) {
                //maybe todo
                result.append('|').append(annotation.tag()).append('=').append(field.get(in));
            } else {
                if (field.get(in) != null) {
                    result.append('|').append(annotation.tag()).append('=').append(setSimpleValue(field, in));
                }
            }
        }
    }

    private static String setSimpleValue(Field field, Object in) throws IllegalArgumentException, IllegalAccessException, ParseException {
        switch (field.getType().getSimpleName()) {
            case "Integer":
            case "String":
            case "int":
            case "Long":
            case "long":
            case "Double":
            case "double":
            case "float":
            case "Float": {
                return String.valueOf(field.get(in));
            }
            case "boolean":
            case "Boolean": {
                //todo
                return toBoolean(field.getBoolean(in), "");
            }
            case "Date": {
                Date date = (Date) field.get(in);
                //todo
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd-HH:mm:ss.SSS");
                return format.format(date);
            }
            default: {
                throw new IllegalArgumentException("Unable to resolve");
            }
        }
    }

    private static String toBoolean(boolean value, String booleanPresent) {
        switch (booleanPresent) {
            case "d":
                return value ? "1" : "0";
            case "s":
                return value ? "Y" : "N";
            default:
                return String.valueOf(value);
        }
    }
}
